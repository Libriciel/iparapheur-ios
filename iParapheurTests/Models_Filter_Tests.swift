/*
 * i-Parapheur iOS
 * Copyright (C) 2012-2020 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import XCTest
@testable import iParapheur


class Models_Filter_Tests: XCTestCase {


    func testToJson() {

        // Prepare

//        let filter = NSEntityDescription.insertNewObject(forEntityName: Filter.EntityName,
//                                                         into: ModelsDataController.Context!) as! Filter
//        filter.id = "test_id"
//        filter.name = "test_name"
//        filter.title = "test_title"
//        filter.typeList = ["test_type_list_1", "test_type_list_2"] as [String]
//        filter.subTypeList = ["test_subtype_list_1", "test_subtype_list_2"] as [String]
//        filter.state = State.EN_COURS.rawValue
//        filter.beginDate = Date(timeIntervalSince1970: 200) as NSDate
//        filter.endDate = Date(timeIntervalSince1970: 400) as NSDate
//
//        // Test
//
//        let jsonEncoder = JSONEncoder()
//        jsonEncoder.dateEncodingStrategy = .iso8601
//
//        let jsonData = try! jsonEncoder.encode(filter)
//        let jsonString = String(data: jsonData, encoding: .utf8)
//
//        // TODO : Proper tests
//        XCTAssertNotNil(jsonString)
//        XCTAssertTrue(jsonString!.count > 50)
//
//        // Cleanup
//
//        ModelsDataController.Context!.delete(filter)
    }

}
